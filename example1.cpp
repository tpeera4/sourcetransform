// #include <string>
// #include <iostream>
// 
// // Declares clang::SyntaxOnlyAction.
// #include "clang/Frontend/FrontendActions.h"
// #include "clang/Tooling/CommonOptionsParser.h"
// #include "clang/Tooling/Tooling.h"
// // Declares llvm::cl::extrahelp.
// #include "llvm/Support/CommandLine.h"
// 
// #include "clang/ASTMatchers/ASTMatchers.h"
// #include "clang/ASTMatchers/ASTMatchFinder.h"
// 
// #include "clang/Tooling/Refactoring.h"
// 
// using namespace clang;
// using namespace clang::tooling;
// using namespace llvm;
// using namespace clang::ast_matchers;
// 
// //clang-query test_inputs/test.cpp --
// //clang -Xclang -ast-dump -fsyntax-only test_inputs/test.cpp --
// 
// // Apply a custom category to all command-line options so that they are the
// // only ones displayed.
// static llvm::cl::OptionCategory MyToolCategory ( "my-tool options" );
// 
// // CommonOptionsParser declares HelpMessage with a description of the common
// // command-line options related to the compilation database and input files.
// // It's nice to have this help message in all tools.
// static cl::extrahelp CommonHelp ( CommonOptionsParser::HelpMessage );
// 
// // A help message for this specific tool can be added afterwards.
// static cl::extrahelp MoreHelp ( "\nMore help text..." );
// 
// DeclarationMatcher functionMatcher = functionDecl().bind ( "funcBinding" );
// 
// class FuncPrinter : public MatchFinder::MatchCallback
// {
// public:
//     virtual void run ( const MatchFinder::MatchResult &Result )
//     {
//         if ( const FunctionDecl *FD = Result.Nodes.getNodeAs<clang::FunctionDecl> ( "funcBinding" ) ) {
// //             FD->dump();
//             FD->getNameInfo().getAsString();
//             for ( auto it : FD->attrs() ) {
//                 Attr& attr = ( *it );
//                 auto annotatedAttr = dyn_cast<AnnotateAttr> ( &attr );
//                 std::cout << std::string ( annotatedAttr->getAnnotation() );
//             }
//         }
//     }
// };
// 
// class FuncHandler: public MatchFinder::MatchCallback
// {
// public:
//     FuncHandler ( Replacements *Replace ) : Replace ( Replace ) {}
// 
//     virtual void run ( const MatchFinder::MatchResult &Result )
//     {
//         if ( const FunctionDecl *FD = Result.Nodes.getNodeAs<clang::FunctionDecl> ( "funcBinding" ) ) {
// 
//             Replacement Rep (*(Result.SourceManager), FD->getLocStart(), 0,
//                               "// the 'function' part\n" );
//             Replace->add (Rep);
//         }
//     }
// 
// private:
//     Replacements *Replace;
// 
// };
// 
// 
// 
// int main ( int argc, const char **argv )
// {
//     CommonOptionsParser OptionsParser ( argc, argv, MyToolCategory );
//     ClangTool Tool ( OptionsParser.getCompilations(),
//                      OptionsParser.getSourcePathList() );
// 
//     RefactoringTool RefTool ( OptionsParser.getCompilations(), OptionsParser.getSourcePathList() );
// //     FuncHandler HandlerFunc ( (Replacements *)RefTool.getReplacements() );
// 
//     FuncPrinter FunctionPrinter;
//     MatchFinder Finder;
// 
//     Finder.addMatcher ( functionMatcher, &FunctionPrinter );
//     return Tool.run ( newFrontendActionFactory ( &Finder ).get() );
// }
