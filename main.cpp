//===---- tools/extra/ToolTemplate.cpp - Template for refactoring tool ----===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
//  This file implements an empty refactoring tool using the clang tooling.
//  The goal is to lower the "barrier to entry" for writing refactoring tools.
//
//  Usage:
//  tool-template <cmake-output-dir> <file1> <file2> ...
//
//  Where <cmake-output-dir> is a CMake build directory in which a file named
//  compile_commands.json exists (enable -DCMAKE_EXPORT_COMPILE_COMMANDS in
//  CMake to get this output).
//
//  <file1> ... specify the paths of files in the CMake source tree. This path
//  is looked up in the compile command database. If the path of a file is
//  absolute, it needs to point into CMake's source tree. If the path is
//  relative, the current working directory needs to be in the CMake source
//  tree and the file must be in a subdirectory of the current working
//  directory. "./" prefixes in the relative files will be automatically
//  removed, but the rest of a relative path must be a suffix of a path in
//  the compile command line database.
//
//  For example, to use tool-template on all files in a subtree of the
//  source tree, use:
//
//    /path/in/subtree $ find . -name '*.cpp'|
//        xargs tool-template /path/to/build
//
//===----------------------------------------------------------------------===//

#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/Basic/SourceManager.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Lex/Lexer.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Refactoring.h"
#include "clang/Tooling/Tooling.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/MemoryBuffer.h"
#include "llvm/Support/Signals.h"
#include "clang/Frontend/TextDiagnosticPrinter.h"
#include <clang/Rewrite/Core/Rewriter.h>

using namespace clang;
using namespace clang::ast_matchers;
using namespace clang::tooling;
using namespace llvm;

using clang::tooling::Replacement;
using clang::tooling::RefactoringTool;
using clang::tooling::Replacement;
using clang::tooling::CompilationDatabase;
using clang::tooling::newFrontendActionFactory;

DeclarationMatcher functionMatcher = functionDecl().bind ( "funcBinding" );
namespace
{

class FunctionCallback : public MatchFinder::MatchCallback
{
public:
    FunctionCallback ( std::map<std::string, Replacements> *Replace )
        : Replace ( Replace ) {}

    void run ( const MatchFinder::MatchResult &Result ) override
    {
        if ( const FunctionDecl *func = Result.Nodes.getNodeAs<clang::FunctionDecl> ( "funcBinding" ) ) {

            for ( auto it : func->attrs() ) {
                Attr& attr = ( *it );
                auto annotatedAttr = dyn_cast<AnnotateAttr> ( &attr );
                llvm::outs() << std::string ( annotatedAttr->getAnnotation() );
                if ( annotatedAttr->getAnnotation() =="secure_func" ) {
                    Replacement Rep ( * ( Result.SourceManager ), func->getLocStart(), 0,
                                      "// -Secure function- \n" );

                    llvm::outs() << func->getNameInfo().getAsString();

                    if ( ( *Replace ).count ( Rep.getFilePath() ) ==0 ) {
                        tooling::Replacements Replacements;
                        ( *Replace ) [Rep.getFilePath()] = Replacements;
                    }

                    auto Err = ( *Replace ) [Rep.getFilePath()].add ( Rep );
                    if ( Err ) {
                        llvm::errs() << llvm::toString ( std::move ( Err ) ) << "\n";
                        abort();
                    }
                }
            }
        }
    }

private:
    std::map<std::string, Replacements> *Replace;
};
} // end anonymous namespace

// Set up the command line options
static cl::extrahelp CommonHelp ( CommonOptionsParser::HelpMessage );
static cl::OptionCategory ToolTemplateCategory ( "tool-template options" );

int main ( int argc, const char **argv )
{
    llvm::sys::PrintStackTraceOnErrorSignal ( argv[0] );
    CommonOptionsParser OptionsParser ( argc, argv, ToolTemplateCategory );
    RefactoringTool Tool ( OptionsParser.getCompilations(),
                           OptionsParser.getSourcePathList() );
    ast_matchers::MatchFinder Finder;
    FunctionCallback Callback ( &Tool.getReplacements() );

    Finder.addMatcher ( functionMatcher, &Callback );

//     Tool.run ( newFrontendActionFactory ( &Finder ).get() );
    Tool.runAndSave ( newFrontendActionFactory ( &Finder ).get() );



    for ( auto &r : Tool.getReplacements() ) {
        llvm::outs() << r.first  << "\n";
        auto replacements = r.second;
        for ( auto &rf : replacements ) {
            llvm::errs() << rf.getReplacementText() << "\n";

        }

    }
    // We need a SourceManager to set up the Rewriter.
    IntrusiveRefCntPtr<DiagnosticOptions> DiagOpts = new DiagnosticOptions();
    DiagnosticsEngine Diagnostics (
        IntrusiveRefCntPtr<DiagnosticIDs> ( new DiagnosticIDs() ), &*DiagOpts,
        new TextDiagnosticPrinter ( llvm::errs(), &*DiagOpts ), true );

    SourceManager Sources ( Diagnostics, Tool.getFiles() );

    // Apply all replacements to a rewriter.
    Rewriter Rewrite ( Sources, LangOptions() );
    bool res = Tool.applyAllReplacements ( Rewrite );
    llvm::outs() << res;

    return 0;
}
